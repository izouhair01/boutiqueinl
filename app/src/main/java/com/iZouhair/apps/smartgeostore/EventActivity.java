package com.droideve.apps.smartgeostore;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.droideve.apps.smartgeostore.GPS.GPStracker;
import com.droideve.apps.smartgeostore.Notifcation_Receiver.AlarmReceiver;
import com.droideve.apps.smartgeostore.classes.Event_c;
import com.droideve.apps.smartgeostore.classes.Images;
import com.droideve.apps.smartgeostore.classes.Store;
import com.droideve.apps.smartgeostore.database.DatabaseAdapter;
import com.droideve.apps.smartgeostore.load_manager.ViewManager;
import com.droideve.apps.smartgeostore.parser.api_parser.ImagesParser;
import com.droideve.apps.smartgeostore.utils.Utils;
import com.ecloud.pulltozoomview.PullToZoomScrollViewEx;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Idriss on 05/10/2016.
 */

public class EventActivity extends AppCompatActivity implements ViewManager.CustomView ,OnMapReadyCallback {

    private static final DateFormat FORMATTER = SimpleDateFormat.getDateInstance();

    private PullToZoomScrollViewEx scrollView;
    Toolbar toolbar;
    GoogleMap mMap;
    GoogleDirection gd;
    Document mDoc;
    Timer timer;

    private TextView APP_TITLE_VIEW = null;
    private TextView APP_DESC_VIEW = null;
    private DatabaseAdapter database;
    Context context;
    public ViewManager mViewManager;
    Event_c eventData;
    List<String> listImages;
    FloatingActionButton fab;

    private AdView mAdView;


    private int statusBarColor;
    private ImageView image;
    private TextView name , description , tel , website ,date;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.event_activity);



        context = this;

        initToolbar();





        mViewManager = new ViewManager(this);
        mViewManager.setLoading(findViewById(R.id.loading));
        mViewManager.setNoLoading(findViewById(R.id.no_loading));
        mViewManager.setError(findViewById(R.id.error));
        mViewManager.setCustumizeView(this);
        mViewManager.showResult();


        invalidateOptionsMenu();



        scrollView = (PullToZoomScrollViewEx) findViewById(R.id.scroll_view);


        loadViewForCode();


        new getEventData().execute();


        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapping);
        mapFrag.getMapAsync(this);

        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        int mScreenHeight = localDisplayMetrics.heightPixels;
        int mScreenWidth = localDisplayMetrics.widthPixels;
        LinearLayout.LayoutParams localObject = new LinearLayout.LayoutParams(mScreenWidth, (int) (9.0F * (mScreenWidth / 16.0F)));
        scrollView.setHeaderLayoutParams(localObject);



        //INITIALZE THE DATABASE
        database = new DatabaseAdapter(this);


        //Floating Action Button
        fab = (FloatingActionButton) findViewById(R.id.fab);




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action



                if(eventData != null)
                {



                   if(!database.isEventLiked(eventData.getId())) //CHECK IS THIS EVENT IS ALLREADY LIKED
                   {
                       database.likeEvent(eventData.getId(),eventData.getName(),eventData.getDateB(),0);


                       //ENABLE ALARM MANAGER TO START
                       try {
                           SetAlert(eventData.getDateB(),eventData.getName());
                       } catch (ParseException e) {
                           e.printStackTrace();
                       }


                       Toast.makeText(getApplicationContext(), "Succesfully saved !!", Toast.LENGTH_SHORT).show();

                   }else
                   {
                       database.dislikedEvent(eventData.getId());

                       Toast.makeText(getApplicationContext(), "Event Disliked !!", Toast.LENGTH_SHORT).show();

                   }

                    initializeBtn();
                }




            }
        });

    }






    private void initializeBtn()
    {
        if(!database.isEventLiked(eventData.getId()))
        {
            fab.setImageResource(R.drawable.ic_favorite_border_white_24dp);

        }else
        {
            fab.setImageResource(R.drawable.ic_favorite_white_24dp);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();



    }

    @Override
    protected void onResume() {

        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();

    }

    @Override
    public void customErrorView(View v) {

    }

    @Override
    public void customLoadingView(View v) {

    }

    @Override
    public void customEmptyView(View v) {

    }


    public void initToolbar(){

        toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setSubtitle("E-shop");
        getSupportActionBar().setTitle("");
        //getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_36dp);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(false);


        getSupportActionBar().setDisplayShowTitleEnabled(false);
        APP_TITLE_VIEW = (TextView) toolbar.findViewById(R.id.toolbar_title);
        APP_DESC_VIEW = (TextView) toolbar.findViewById(R.id.toolbar_description);

        APP_DESC_VIEW.setVisibility(View.GONE);
        Utils.setFont(this, APP_DESC_VIEW, "SourceSansPro-Black.otf");
        Utils.setFont(this, APP_TITLE_VIEW , "SourceSansPro-Black.otf");

        APP_TITLE_VIEW.setText("Detail Event");

        APP_DESC_VIEW.setVisibility(View.GONE);

    }

    private void loadViewForCode() {
        View headView = LayoutInflater.from(this).inflate(R.layout.event_head_view, null, false);

        View zoomView = LayoutInflater.from(this).inflate(R.layout.event_zoom_view, null, false);

        image = (ImageView)zoomView.findViewById(R.id.iv_zoom);


        View contentView = LayoutInflater.from(this).inflate(R.layout.event_content_view, null, false);
        //name = (TextView) contentView.findViewById(R.id.event_name);
        description = (TextView) contentView.findViewById(R.id.event_desc);
        tel = (TextView) contentView.findViewById(R.id.event_tel);
        website = (TextView) contentView.findViewById(R.id.event_website);
        date = (TextView) contentView.findViewById(R.id.event_date);


        //ADD ADMOB BANNER
        mAdView = (AdView) contentView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);



        // Utils.setFont(this, name, "");
        Utils.setFont(this, tel, "");
        Utils.setFont(this,description,"");
        Utils.setFont(this, website, "");
        Utils.setFont(this, date, "");


        scrollView.setZoomView(zoomView);
        scrollView.setScrollContentView(contentView);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap = googleMap;
        if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
            Log.e("GooglePlayServices","Available");
            System.gc(); mMap.clear();}

        initMapping();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    private class getEventData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mViewManager.loading();
            eventData = new Event_c();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mViewManager.showResult();

            if (!eventData.getName().equals("")) {



//                if (storedata.getLatitude() == 0 && storedata.getLongitude() == 0) {
//                    image.setVisibility(View.VISIBLE);
//                    findViewById(R.id.mapcontainer).setVisibility(View.GONE);
//                }


                if(listImages.size()>0) {

                    Log.e("Images",listImages.get(0));
                    findViewById(R.id.mapcontainer).setVisibility(View.VISIBLE);
                    Picasso.with(getBaseContext())
                            .load(eventData.getImages().getUrl500_500())
                            .fit().centerCrop().placeholder(R.drawable.def_logo)
                            .into(image);
                }else {

                    Picasso.with(getBaseContext())
                            .load(R.drawable.def_logo)
                            .fit().centerCrop().placeholder(R.drawable.def_logo)
                            .into(image);

                   // initMapping();
                }



                APP_TITLE_VIEW.setText(eventData.getName());


                //address.setText(eventData.getAddress());

                description.setText(Html.fromHtml(eventData.getDescription()));
                date.setText(" From : "+eventData.getDateB()+"  to : "+eventData.getDateE()+" \n Address : "+eventData.getAddress());

                if(eventData.getTel().length()>0)
                {
                    tel.setText(eventData.getTel());
                }else
                {
                    tel.setClickable(false);
                    tel.setText(" Unknoun");

                }

                if(eventData.getWebSite().length()>0)
                {
                    website.setText(eventData.getWebSite());
                }else
                {

                    website.setText(" Unknoun");

                }



                tel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            Intent intent = new Intent(Intent.ACTION_CALL);
                            intent.setData(Uri.parse("tel:" + eventData.getTel().trim()));
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(getApplicationContext(), "Error in your phone call" + e.getMessage(), Toast.LENGTH_LONG).show();
                        }

                    }
                });


                website.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(eventData.getWebSite().trim()!="" && eventData.getWebSite().length()>0 )
                        {

                            WebSiteDialogFragment webDialog = new WebSiteDialogFragment();


                            Bundle args = new Bundle();
                            args.putString("url",eventData.getWebSite());
                            args.putString("name",eventData.getName());

                            webDialog.setArguments(args);

                            webDialog.show(getSupportFragmentManager(), "test-normal");
                        }
                    }
                });



                initializeBtn();

            }else{
                finish();
            }




        }

        @Override
        protected Void doInBackground(Void... params) {


            Bundle bundle = getIntent().getExtras();


            if(bundle!=null){





                try {


                    JSONObject js = new JSONObject( getIntent().getExtras().getString(Event_c.Tags.LISTIMAGES));


                    JSONObject objImage = js.getJSONObject("images");
                    Log.e("ImageObj",objImage.length()+"");
                    listImages = new ArrayList<String>();
                    for(int i=0;i<objImage.length()+1;i++){
                        listImages.add(objImage.getString(i+""));
                        Log.e("listImage",objImage.getString(i+""));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                eventData.setId(bundle.getInt(Event_c.Tags.ID));
                eventData.setName(bundle.getString(Event_c.Tags.NAME));
                eventData.setAddress(bundle.getString(Event_c.Tags.ADDRESS));
                eventData.setDistance(bundle.getDouble(Event_c.Tags.DISTANCE));
                eventData.setLat(bundle.getDouble(Event_c.Tags.LAT));
                eventData.setLng(bundle.getDouble(Event_c.Tags.LONG));
                eventData.setStatus(bundle.getInt(Event_c.Tags.STATUS));
                eventData.setType(bundle.getInt(Event_c.Tags.TYPE));
                eventData.setDescription(bundle.getString(Event_c.Tags.DESCRIPTION));
                eventData.setTel(bundle.getString(Event_c.Tags.TEL));
                eventData.setDateB(bundle.getString(Event_c.Tags.DATE_B));
                eventData.setDateE(bundle.getString(Event_c.Tags.DATE_E));
                eventData.setWebSite(bundle.getString(Event_c.Tags.WEBSITE));






                try {



                    String jsf = bundle.getString(Store.Tags.LISTIMAGES);

                    JSONObject js = new JSONObject(jsf);
                    JSONObject objImage = js.getJSONObject("images");;
                    ImagesParser mImagesParser = new ImagesParser(objImage);
                    List<Images> images = mImagesParser.getImagesList();
                    if (images.size() > 0)
                        eventData.setImages(images.get(0));
                    Log.e("eventData",eventData.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e){
                    e.printStackTrace();
                }


            }

            return null;
        }
    }


    LatLng customerPosition;
    LatLng myPosition;

    private void initMapping() {


        if(eventData.getLat() != null && eventData.getLng() != null){


            double TraderLat = eventData.getLat();
            double TraderLng = eventData.getLng();

            customerPosition = new LatLng(TraderLat, TraderLng);

            //INITIALIZE MY LOCATION
            GPStracker trackMe = new GPStracker(this);

            myPosition = new LatLng(trackMe.getLatitude(), trackMe.getLongitude());

        /*mMap = ((SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapping)).getMap();
*/



            if (mMap != null) {

                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(customerPosition, 17));
                //trader location

                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_flag_black_24dp);
                mMap.addMarker(new MarkerOptions().position(customerPosition)
                        .title(eventData.getName())
                        .anchor(0.0f, 1.0f)
                        .icon(icon)
                        .snippet(eventData.getAddress())).showInfoWindow();

                mMap.addMarker(new MarkerOptions().position(myPosition)
                        .title(eventData.getName())
                        .anchor(0.0f, 1.0f)
                        .draggable(true)
                        //.icon(icon)
                        .snippet(eventData.getAddress())).showInfoWindow();


                gd = new GoogleDirection(this);


                //My Location
                gd.setOnDirectionResponseListener(new GoogleDirection.OnDirectionResponseListener() {

                    @Override
                    public void onResponse(String status, Document doc, GoogleDirection gd) {
                        mDoc = doc;
                        mMap.addPolyline(gd.getPolyline(doc, 5, getResources().getColor(R.color.colorPrimary)));
                        //mMap.setMyLocationEnabled(true);
                    }
                });


                gd.setLogging(true);
                gd.request(myPosition, customerPosition, GoogleDirection.MODE_DRIVING);

                try {
                    TimerTask task = new TimerTask() {
                        @Override
                        public void run() {
                        /*
                        if (ServiceHandler.isNetworkAvailable(getApplicationContext())) {

                            gd.setCameraUpdateSpeed(10);
                        }*/
                        }
                    };
                    timer = new Timer();
                    timer.scheduleAtFixedRate(task, 0, 6000);
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        }


           }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        menu.findItem(R.id.event_ico).setVisible(true);
        menu.findItem(R.id.share_ico).setVisible(true);



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }else if(item.getItemId() == R.id.event_ico)
        {

            SimpleCalendarDialogFragment calendarFragment = new SimpleCalendarDialogFragment();
            Bundle params = new Bundle();
            params.putString("firstDate",eventData.getDateB());
            params.putString("lastDate",eventData.getDateE());
            calendarFragment.setArguments(params);

            calendarFragment.show(getSupportFragmentManager(), "test-normal");


            //new SimpleCalendarDialogFragment().show();
        }else if (item.getItemId()   == R.id.share_ico)
        {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    "Detail Event   : \n   " + eventData.getName() + "  \n Address  : \n  " + eventData.getAddress() + "  \n Date Event : \n  from   " + eventData.getDateB() + "    to  "+eventData.getDateE()+"  " +
                            " \n Download : \n   \"" + getString(R.string.app_name) + "\" " + Constances.PLAY_STORE_URL + "\n" +
                    "");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }

        return super.onOptionsItemSelected(item);
    }



    //Dispaly the calendar in Dialog Fragment
    public static class SimpleCalendarDialogFragment extends AppCompatDialogFragment implements OnDateSelectedListener {


        private Date fisrtDate ;
        private Date lastDate ;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle bundle = getArguments();
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            try {
                fisrtDate = formater.parse(bundle.getString("firstDate"));
                lastDate = formater.parse(bundle.getString("lastDate"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            LayoutInflater inflater = getActivity().getLayoutInflater();

            //inflate custom layout and get views
            //pass null as parent view because will be in dialog layout
            View view = inflater.inflate(R.layout.event_calendar, null);






            MaterialCalendarView widget = (MaterialCalendarView) view.findViewById(R.id.calendarView);

            //widget.setOnDateChangedListener(this);


            widget.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
            /*widget.setDateSelected(CalendarDay.from(2016, 4, 3),true);
            widget.setDateSelected(CalendarDay.from(2016, 4, 4),true);
            widget.setDateSelected(CalendarDay.from(2016, 4, 5),true);*/

            if(substractDates(lastDate,fisrtDate)>=0) {
                widget.setCurrentDate(CalendarDay.from(fisrtDate)); //locate the calendar in the first day of the date
                dispatchOnRangeSelected(widget, CalendarDay.from(fisrtDate), CalendarDay.from(lastDate));
            }else
            {
                Toast.makeText(getContext(),"The date is not valid !! for more help contact your support ",Toast.LENGTH_SHORT).show();
            }

            return new AlertDialog.Builder(getActivity())
                    .setTitle("Event Calendar")
                    .setView(view)
                    .setPositiveButton(android.R.string.ok, null)
                    .create();
        }

        @Override
        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
            Toast.makeText(getContext(),FORMATTER.format(date.getDate()),Toast.LENGTH_LONG).show();
        }


        /**
         * Dispatch a range of days to a listener, if set. First day must be before last Day.
         *
         * @param firstDay first day enclosing a range
         * @param lastDay  last day enclosing a range
         */
        protected void dispatchOnRangeSelected(MaterialCalendarView widget,final CalendarDay firstDay, final CalendarDay lastDay) {

            final List<CalendarDay> days = new ArrayList<>();

            final Calendar counter = Calendar.getInstance();
            counter.setTime(firstDay.getDate());  //  start from the first day and increment

            final Calendar end = Calendar.getInstance();
            end.setTime(lastDay.getDate());  //  for comparison

            while (counter.before(end) || counter.equals(end)) {
                final CalendarDay current = CalendarDay.from(counter);
                widget.setDateSelected(current, true);
                days.add(current);
                counter.add(Calendar.DATE, 1);
            }


        }


        private Long substractDates(Date date1, Date date2)
        {
            long restDatesinMillis = date1.getTime()-date2.getTime();
            return  restDatesinMillis;
        }
    }


    public static class WebSiteDialogFragment extends AppCompatDialogFragment implements OnDateSelectedListener {

        private String url;
        private String name;

        private ProgressDialog progressBar;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            LayoutInflater inflater = getActivity().getLayoutInflater();

            url = getArguments().getString("url");
            name = getArguments().getString("name");


            Log.e("WEBVIEW","Url   : "+url);
            Log.e("WEBVIEW","Name  : "+name);
            //inflate custom layout and get views
            //pass null as parent view because will be in dialog layout
            View view = inflater.inflate(R.layout.webview_dialog, null);



            WebView widget = (WebView) view.findViewById(R.id.display_site);

            WebSettings settings = widget.getSettings();
            settings.setJavaScriptEnabled(true);
            widget.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
            final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
            progressBar = ProgressDialog.show(getContext(), "WebView Example", "Loading...");

            widget.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.i("WebSiteDialog", "Processing webview url click...");
                    view.loadUrl(url);
                    return true;
                }

                public void onPageFinished(WebView view, String url) {
                    Log.i("WebSiteDialog", "Finished loading URL: " +url);
                    if (progressBar.isShowing()) {
                        progressBar.dismiss();
                    }
                }

                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    Log.e("WebSiteDialog", "Error: " + description);
                    // Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage(description);
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
                    alertDialog.show();
                }
            });
            widget.loadUrl(url);

            //widget.setOnDateChangedListener(this);


            return new AlertDialog.Builder(getActivity())
                    .setTitle(name)
                    .setView(view)
                    .setPositiveButton(android.R.string.ok, null)
                    .create();
        }

        @Override
        public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
            Toast.makeText(getContext(),FORMATTER.format(date.getDate()),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {

        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }




    private int getStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return getWindow().getStatusBarColor();
        }
        return 0;
    }

    private void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }



    private  void SetAlert(String DateEvent , String Message) throws ParseException {
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");;
        Date  EventNotifDate = formater.parse(DateEvent);
        Calendar time = Calendar.getInstance();
        time.setTime(EventNotifDate);
        time.add(Calendar.DATE,-1);
        AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("message",Message);
        intent.putExtra("dateEvent",DateEvent);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        /*Calendar time = Calendar.getInstance();
        time.setTimeInMillis(System.currentTimeMillis());
        time.add(Calendar.SECOND, 30);*/


        alarmMgr.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), pendingIntent);

        Log.e("Alert","Alert Enabled !");
    }

}
