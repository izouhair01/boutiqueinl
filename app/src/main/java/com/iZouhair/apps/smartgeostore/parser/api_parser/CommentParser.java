package com.droideve.apps.smartgeostore.parser.api_parser;

import com.droideve.apps.smartgeostore.classes.Comments;
import com.droideve.apps.smartgeostore.parser.Parser;
import com.droideve.apps.smartgeostore.parser.tags.Tags;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idriss on 18/07/2016.
 */

public class CommentParser  extends Parser {
    public CommentParser(JSONObject json) {
        super(json);
    }


    public List<Comments> getComments()
    {


        List<Comments> list = new ArrayList<Comments>();



            try {
            JSONObject json_array = json.getJSONObject(Tags.RESULT);

                for (int i=0;i<json_array.length();i++) {



                    JSONObject json_user = json_array.getJSONObject(i + "");
                    Comments comments = new Comments();
                        String rev = json_user.getString("review");
                        String pse = json_user.getString("pseudo");

                        if(rev == null){ rev="inconnu";}
                        if(pse == null){ pse="no comment found !!";}
                    comments.setComment(rev);
                    comments.setReviewer(pse);

                    list.add(comments);

                }


                } catch (JSONException e) {
            e.printStackTrace();
        }


        return  list;
    }
}
