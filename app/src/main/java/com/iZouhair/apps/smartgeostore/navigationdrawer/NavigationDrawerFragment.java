package com.droideve.apps.smartgeostore.navigationdrawer;


import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.droideve.apps.smartgeostore.AboutActivity;
import com.droideve.apps.smartgeostore.CategoriesActivity;
import com.droideve.apps.smartgeostore.EventLikedActivity;
import com.droideve.apps.smartgeostore.FavoriteStoreActivity;
import com.droideve.apps.smartgeostore.R;
import com.droideve.apps.smartgeostore.SettingActivity;
import com.droideve.apps.smartgeostore.adapter.navigation.SimpleListAdapterNavDrawer;
import com.droideve.apps.smartgeostore.classes.HeaderItem;
import com.droideve.apps.smartgeostore.classes.Item;
import com.droideve.apps.smartgeostore.classes.User;
import com.droideve.apps.smartgeostore.database.DatabaseAdapter;
import com.droideve.apps.smartgeostore.fragments.MainFragment;
import com.droideve.apps.smartgeostore.network.VolleySingleton;

import net.steamcrafted.materialiconlib.MaterialDrawableBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class NavigationDrawerFragment extends Fragment implements SimpleListAdapterNavDrawer.ClickListener {


    private  static  final  int RESULT_SART_ACTIVITY = 1;



    public static final String PREF_FILE_NAME = "testpref";
    public static  final  String KEY_USER_LEARNED_DRAWER = "learned_user_drawer";




    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private boolean mUserLearedLayout;
    private boolean mFromSaveInstanceState;
    private View containerView;


    private RecyclerView drawerList;
   private SimpleListAdapterNavDrawer adapter;





    public void initUIL(){

    }


    public NavigationDrawerFragment(){

    }

    private DatabaseAdapter database;
    private User user;

    //init request http
    private RequestQueue queue;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        queue = VolleySingleton.getInstance(getActivity()).getRequestQueue();
        initUIL();

        mUserLearedLayout = Boolean.valueOf(readFromPreferences(getActivity(),KEY_USER_LEARNED_DRAWER,"false")) ;
        if(savedInstanceState!=null){
            mFromSaveInstanceState = true;
        }


        database = new DatabaseAdapter(getActivity());

        user = database.getUser();


    }

    public TextView Username;
    public TextView Email;



    Fragment frag;
    FragmentManager fm;


    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.navigation_drawer_content,container,false);



        rootView.setClickable(true);






        drawerList = (RecyclerView) rootView.findViewById(R.id.drawerLayout);
        drawerList.setVisibility(View.VISIBLE);

        adapter = new SimpleListAdapterNavDrawer(getActivity(),getData());

        drawerList.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        drawerList.setLayoutManager(mLayoutManager);
        drawerList.setAdapter(adapter);

        adapter.setClickListener(this);



        return rootView;

    }



    List<Item> listItems = Arrays.asList();

    public List<Item> getData(){

        listItems = new ArrayList<Item>();


        HeaderItem header_item = new HeaderItem();
        header_item.setName(getResources().getString(R.string.Home));
        header_item.setEmail(getResources().getString(R.string.your_email));

        header_item.setEnabled(false);


        Item homeItem =  new Item();
        homeItem.setName(getResources().getString(R.string.Home));
        homeItem.setIconDraw(MaterialDrawableBuilder.IconValue.HOME);




        int categories_count = database.getCats().size();
        Item catItem = new Item();
        catItem.setName(getResources().getString(R.string.Categories)+" ("+categories_count+")");
        catItem.setIconDraw(MaterialDrawableBuilder.IconValue.FORMAT_LIST_BULLETED);


        int bookmaeks_count = database.getStoresSavedCount();
        Item savesItem = new Item();
        savesItem.setName(getResources().getString(R.string.Favoris)+" ("+bookmaeks_count+")");
        savesItem.setIconDraw(MaterialDrawableBuilder.IconValue.BOOKMARK_CHECK);


        Item EventLikeItem =new Item();
        EventLikeItem.setName(getResources().getString(R.string.EventLike));
        EventLikeItem.setIconDraw(MaterialDrawableBuilder.IconValue.ALARM_CHECK);



        Item aboutItem =new Item();
        aboutItem.setName(getResources().getString(R.string.about));
        aboutItem.setIconDraw(MaterialDrawableBuilder.IconValue.INFORMATION_OUTLINE);


        Item settingItem =new Item();
        settingItem.setName(getResources().getString(R.string.Settings));
        settingItem.setIconDraw(MaterialDrawableBuilder.IconValue.SETTINGS);








        Item userItem =new Item();

        if(user!=null){


            userItem.setName(getResources().getString(R.string.Logout));
            userItem.setIconDraw(MaterialDrawableBuilder.IconValue.LOGOUT);


        }else{


            userItem.setName(getResources().getString(R.string.Login));
            userItem.setIconDraw(MaterialDrawableBuilder.IconValue.ACCOUNT);

        }





        if(header_item.isEnabled())
            listItems.add(header_item);

        if(homeItem.isEnabled())
            listItems.add(homeItem);

        if(catItem.isEnabled())
            listItems.add(catItem);

        if(savesItem.isEnabled())
            listItems.add(savesItem);

        if(EventLikeItem.isEnabled())
            listItems.add(EventLikeItem);

        if(settingItem.isEnabled())
            listItems.add(settingItem);

        if(aboutItem.isEnabled())
            listItems.add(aboutItem);



        /*
        if(userItem.isEnabled())
            listItems.add(userItem);
    */

        return  listItems;
    }


    public static void saveToPreferences(Context context,String preferenceName,String preferenceValue){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(preferenceName, preferenceValue);
        edit.apply();

    }

    public static String readFromPreferences(Context context,String preferenceName,String defaultValue){

        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName,defaultValue);

    }


    public void setUp(int FragId,DrawerLayout drawerlayout, final Toolbar toolbar){

        containerView = getView().findViewById(FragId);
        mDrawerLayout = drawerlayout;

        //mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);

        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),
                drawerlayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close
                ){
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
                if(!mUserLearedLayout){
                    mUserLearedLayout = true;
                    saveToPreferences(getActivity(),KEY_USER_LEARNED_DRAWER,mUserLearedLayout+"");
                }


                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();


            }



            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);


            }
        };

        if(!mUserLearedLayout && !mFromSaveInstanceState){

            mDrawerLayout.openDrawer(containerView);
        }



        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==INT_CHAT_BOX){

            adapter.getData().get(1).setNotify(0);
            adapter.update(1,adapter.getData().get(1));

        }
    }

    public static int INT_CHAT_BOX = 5;



    @Override
    public void itemClicked(View view, int position) {


        MainFragment mf = (MainFragment)getFragmentManager().findFragmentByTag(MainFragment.TAG);


        switch (position){
            case 0:
                if(mDrawerLayout!=null)
                    mDrawerLayout.closeDrawers();
                mf.setCurrentFragment(0);
                break;
            case 1:

                startActivity(new Intent(getActivity(), CategoriesActivity.class));
                getActivity().overridePendingTransition(R.anim.lefttoright_enter, R.anim.lefttoright_exit);

                break;
            case 2:


               /* if(mDrawerLayout!=null)
                    mDrawerLayout.closeDrawers();
                mf.setCurrentFragment(Constances.initConfig.ListCats.size()-2);*/


                startActivity( new Intent(getActivity(), FavoriteStoreActivity.class));
                getActivity().overridePendingTransition(R.anim.lefttoright_enter, R.anim.lefttoright_exit);
                break;
            case 3:

                startActivity( new Intent(getActivity(), EventLikedActivity.class));
                getActivity().overridePendingTransition(R.anim.lefttoright_enter, R.anim.lefttoright_exit);


                break;
            case 4:



                startActivity(new Intent(getActivity(), SettingActivity.class));
                getActivity().overridePendingTransition(R.anim.lefttoright_enter, R.anim.lefttoright_exit);


                break;
            case 5:

                startActivity( new Intent(getActivity(), AboutActivity.class));
                getActivity().overridePendingTransition(R.anim.lefttoright_enter, R.anim.lefttoright_exit);

                break;
            case 6:


                break;

        }



    }







}
