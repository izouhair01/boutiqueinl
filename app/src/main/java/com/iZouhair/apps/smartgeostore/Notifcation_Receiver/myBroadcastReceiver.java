package com.droideve.apps.smartgeostore.Notifcation_Receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.droideve.apps.smartgeostore.Constances;
import com.droideve.apps.smartgeostore.GPS.GPStracker;
import com.droideve.apps.smartgeostore.MainActivity;
import com.droideve.apps.smartgeostore.R;
import com.droideve.apps.smartgeostore.classes.Store;
import com.droideve.apps.smartgeostore.fragments.SettingsFragment;
import com.droideve.apps.smartgeostore.network.ServiceHandler;
import com.droideve.apps.smartgeostore.network.VolleySingleton;
import com.droideve.apps.smartgeostore.network.api_request.SimpleRequest;
import com.droideve.apps.smartgeostore.parser.api_parser.StoreParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by idriss on 05/07/2016.
 */


public class myBroadcastReceiver extends BroadcastReceiver {


    String Message;
    NotificationCompat.Builder  builder;


    // Check for network availability
    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }



    @Override
    public void onReceive(Context context, Intent intent) {


        GPStracker gps = new GPStracker(context);


        if (ServiceHandler.isNetworkAvailable(context)) {


            //NOTIFY USER WHEN THERE IS A STORE NEAR HIM

            if (SettingsFragment.isNotifyNearTrue(context)
                    && gps.canGetLocation()) {

                SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);

                boolean Notif_NearBy = sh.getBoolean("KEY_PREF_NOTIFY_NEAR", true);
                if (Notif_NearBy) {
                    getNearStore(context);
                }

            }

            //NOTIFY USER WHEN THERE IS A EVENT IS UP COMMING

           /* SharedPreferences AlertEvent = context.getSharedPreferences("AlertEvent",Context.MODE_PRIVATE);
             String NotifyEvent = AlertEvent.getString("notifEvent",null);
            if(NotifyEvent != null)
            {
                List<EventLiked> myEventLiked =
                        new Gson().fromJson(NotifyEvent, new TypeToken<List<EventLiked>>() {
                        }.getType());

                //TODO: Call ALarmMAnager (Enable alarm when  currentDate  = (dateEvent - (1day)
              for(int i=0;i<myEventLiked.size();i++)
              {

                  SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
                  Calendar c = Calendar.getInstance();
                  String currentDate = formater.format(c.getTime());
                 // if(myEventLiked.get(i).getDATE_EVENT())
                  try {
                      Date  EventNotifDate = formater.parse(myEventLiked.get(i).getDATE_EVENT());
                      Date CurrentDate = formater.parse(currentDate);
                      Long result = substractDates(EventNotifDate,CurrentDate);
                      if(result >=1)  //Check if the Rest of Date equal to 1 day
                      {

                            Notification(context," Event :"+myEventLiked.get(i).getNAME(),result);

                      }

                  } catch (ParseException e) {
                      e.printStackTrace();
                  }
              }
            }else
            {
                Log.e("NotifEventBroadCast","Not Found");
            }

*/

        }


        }


    private Long substractDates(Date date1, Date date2) {
        long restDatesinMillis = date1.getTime() - date2.getTime();
        return restDatesinMillis / (24 * 60 * 60 * 1000);
    }

    public void getNearStore(final Context context) {


        final GPStracker mGPS = new GPStracker(context);
        final RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();


        SimpleRequest request = new SimpleRequest(Request.Method.POST,
                Constances.API.API_USER_GET_STORES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                builder = new NotificationCompat.Builder(context);
                try {


                    JSONObject jsonObject = new JSONObject(response);
                    // Log.e("response", jsonObject.toString());


                    Log.e("response", response);

                    final StoreParser mStoreParser = new StoreParser(jsonObject);


                    (new Handler()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            SharedPreferences sh = PreferenceManager.getDefaultSharedPreferences(context);
                            int raduis = Integer.parseInt(sh.getString("key_notif_radius", "5000"));


                            List<Store> list = mStoreParser.getStore();
                            int count = 0;
                            for (int i = 0; i < list.size(); i++) {
                                if (list.get(i).getDistance() <= raduis) {
                                    Log.e("ListStore :", i + "   " + list.get(i).getName());
                                    count++;
                                }
                            }

                            if (count > 0) {

                                if (count > 10) {
                                    Message = "More than  10 stores  near you  !!";
                                } else {
                                    Message = "Your have " + count + " stores  near you  !!";
                                }
                            }

                            if (Message != null) {
                                builder.setContentText(Message);
                                // builder.setLargeIcon(R.drawable)
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    builder.setStyle(new NotificationCompat.BigTextStyle()
                                            .bigText(Message)
                                            .setSummaryText("Show More"))
                                            .setAutoCancel(true);
                                }


                                builder.setSmallIcon(R.mipmap.ic_launcher);
                                builder.setContentTitle(context.getString(context.getApplicationInfo().labelRes));

                                Intent intent1 = new Intent(context, MainActivity.class);
                                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                                stackBuilder.addParentStack(MainActivity.class);
                                stackBuilder.addNextIntent(intent1);
                                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                                builder.setContentIntent(pendingIntent);

                                NotificationManager NM;
                                NM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                                NM.notify(0, builder.build());


                            }


                        }
                    }, 800);


                } catch (JSONException e) {
                    //send a rapport to support
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if (mGPS.canGetLocation()) {
                    params.put("latitude", mGPS.getLatitude() + "");
                    params.put("longitude", mGPS.getLongitude() + "");


                }


                return params;
            }


        };


        request.setRetryPolicy(new DefaultRetryPolicy(SimpleRequest.TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }


}