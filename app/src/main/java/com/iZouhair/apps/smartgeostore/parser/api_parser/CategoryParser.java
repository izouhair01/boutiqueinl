package com.droideve.apps.smartgeostore.parser.api_parser;

import com.droideve.apps.smartgeostore.classes.category;
import com.droideve.apps.smartgeostore.parser.Parser;
import com.droideve.apps.smartgeostore.parser.tags.Tags;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idriss on 02/07/2016.
 */

public class CategoryParser extends Parser {
    public CategoryParser(JSONObject json) {
        super(json);
    }


    public List<category> getCategories(){

        List<category> list = new ArrayList<category>();

        try{

            JSONObject json_array = json.getJSONObject(Tags.RESULT);

            for (int i=0;i<json_array.length();i++){


                try {
                    JSONObject json_user = json_array.getJSONObject(i + "");
                    category cat = new category();
                    cat.setNumCat(json_user.getInt("id_category"));
                    cat.setNameCat(json_user.getString("name"));
                    cat.setParentCategory(json_user.getInt("parent_id"));


                    list.add(cat);
                }catch (JSONException e){
                    e.printStackTrace();
                }

            }

        }catch (JSONException e){
            e.printStackTrace();
        }


        return list;
    }



}
