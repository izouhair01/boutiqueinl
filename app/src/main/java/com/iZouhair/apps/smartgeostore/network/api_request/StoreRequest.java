package com.droideve.apps.smartgeostore.network.api_request;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

/**
 * Created by idriss on 1/18/2016.
 */
public class StoreRequest extends StringRequest {


    public StoreRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }
}
