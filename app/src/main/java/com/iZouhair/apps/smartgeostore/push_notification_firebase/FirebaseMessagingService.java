package com.droideve.apps.smartgeostore.push_notification_firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.droideve.apps.smartgeostore.MainActivity;
import com.droideve.apps.smartgeostore.R;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by idriss on 06/10/2016.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {


    private static  final  String TAG = "FirebaseMessaging";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG,"From "+remoteMessage.getFrom());

        Log.d(TAG,"Message "+remoteMessage.getData().get("message"));

        showNotification(remoteMessage.getData().get("message"));
    }


    private void showNotification(String message) {

        Intent i = new Intent(this,MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("Smartgeo notification ")
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(1,builder.build());
    }
}
