package com.droideve.apps.smartgeostore.push_notification_firebase;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.droideve.apps.smartgeostore.Constances;
import com.droideve.apps.smartgeostore.network.VolleySingleton;
import com.droideve.apps.smartgeostore.network.api_request.SimpleRequest;
import com.droideve.apps.smartgeostore.parser.tags.Tags;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by idriss on 06/10/2016.
 */

public class FirebaseInstanceIDService  extends FirebaseInstanceIdService {

    private RequestQueue queue;
    private static final  String TAG ="FirebaseInstanceID";

    @Override
    public void onTokenRefresh() {

        registerToken();
    }

    private void registerToken() {

        queue = VolleySingleton.getInstance(this).getRequestQueue();



        SimpleRequest request = new SimpleRequest(Request.Method.POST,
                Constances.API.API_USER_REGISTER_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                try {



                    JSONObject js = new JSONObject(response);

                    Log.d("FirebaseJS",js.toString());


                    int success =Integer.parseInt(js.getString(Tags.SUCCESS));

                    if(success==1) {

                       Log.d("FireBaseInsanceID",js.getString("token"));


                    }else{
                        Log.d("FireBaseInsanceID",js.getString("errors"));

                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                    Map<String,String> errors = new HashMap<String,String>();
                    errors.put("JSONException:", "Try later \"Json parser\"");


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORFitrbase", error.toString());

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                /*
                if(imageObjects!=null)
                params.put("images",imageObjects.toString());
                */


                params.put("token", FirebaseInstanceId.getInstance().getToken());
                Log.e(TAG,"TokenToSend"+ FirebaseInstanceId.getInstance().getToken());
               return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(SimpleRequest.TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);



    }
}
