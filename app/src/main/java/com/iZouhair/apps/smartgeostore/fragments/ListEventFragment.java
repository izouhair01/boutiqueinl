package com.droideve.apps.smartgeostore.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.droideve.apps.smartgeostore.Constances;
import com.droideve.apps.smartgeostore.EventActivity;
import com.droideve.apps.smartgeostore.GPS.GPStracker;
import com.droideve.apps.smartgeostore.R;
import com.droideve.apps.smartgeostore.adapter.lists.EventListAdapter;
import com.droideve.apps.smartgeostore.classes.EventLiked;
import com.droideve.apps.smartgeostore.classes.Event_c;
import com.droideve.apps.smartgeostore.classes.category;
import com.droideve.apps.smartgeostore.database.DatabaseAdapter;
import com.droideve.apps.smartgeostore.dialog.CustomBaseDialog;
import com.droideve.apps.smartgeostore.load_manager.ViewManager;
import com.droideve.apps.smartgeostore.network.ServiceHandler;
import com.droideve.apps.smartgeostore.network.VolleySingleton;
import com.droideve.apps.smartgeostore.network.api_request.SimpleRequest;
import com.droideve.apps.smartgeostore.parser.api_parser.EventParser;
import com.droideve.apps.smartgeostore.parser.tags.Tags;
import com.droideve.apps.smartgeostore.utils.Utils;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by idriss on 2/12/2016.
 */
public class ListEventFragment extends android.support.v4.app.Fragment
        implements EventListAdapter.ClickListener, SwipeRefreshLayout.OnRefreshListener,ViewManager.CustomView {

    private int listType = 1;

    //to check if the event is liked
    private int isLiked = 0;

    private RecyclerView list;
    private EventListAdapter adapter;
    public ViewManager mViewManager;


    //init request http
    private RequestQueue queue;


    //for scrolling params
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;

    LinearLayoutManager mLayoutManager;

    //pager
    private int COUNT = 0;
    private int PAGE = 1;

    private category mCat;


    //loading
    public SwipeRefreshLayout swipeRefreshLayout;

    private GPStracker mGPS;
    private DatabaseAdapter database;
    private   MaterialSearchView searchView;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {


        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);







    }

    @Override
    public void onStart() {


        super.onStart();
        Bundle args = getArguments();
        if(args != null)
        {
            //Toast.makeText(getActivity(), "  is Liked  :"+args.get("isLiked"), Toast.LENGTH_LONG).show();
            isLiked = args.getInt("isLiked");

        }


    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);



        MenuItem item = menu.findItem(R.id.search_icon);
        /*********************************** MaterialSearchView **********************************/
        searchView = (MaterialSearchView) getActivity().findViewById(R.id.search_view);

        if(isLiked ==0) {
            searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {


                @Override
                public boolean onQueryTextSubmit(String query) {

                    PAGE = 1;
                    getEvents(PAGE);
                    Toast.makeText(getActivity(), "   your message :" + search, Toast.LENGTH_LONG).show();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    search = newText;

                    return false;
                }
            });


            searchView.setMenuItem(item);

            searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
                @Override
                public void onSearchViewShown() {


                }


                @Override
                public void onSearchViewClosed() {
                    //Do some magic
                }
            });
            //////////////////////////////////////////////////////////////////////////////////////////////////


        }else
        {
            item.setVisible(false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.store_list_fragment, container, false);








        mGPS = new GPStracker(getActivity());
        database =new DatabaseAdapter(getActivity());


        if(!mGPS.canGetLocation() && listType==1)
            mGPS.showSettingsAlert();


        queue = VolleySingleton.getInstance(getActivity()).getRequestQueue();


        mViewManager = new ViewManager(getActivity());
        mViewManager.setLoading(rootView.findViewById(R.id.loading));
        mViewManager.setNoLoading(rootView.findViewById(R.id.no_loading));
        mViewManager.setError(rootView.findViewById(R.id.error));
        mViewManager.setEmpty(rootView.findViewById(R.id.empty));

        mViewManager.setCustumizeView(this);

        mViewManager.loading();



        list = (RecyclerView) rootView.findViewById(R.id.list);


        adapter = new EventListAdapter(getActivity(), getData());
        adapter.setClickListener(this);


        list.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        //listcats.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        list.setItemAnimator(new DefaultItemAnimator());
        list.setLayoutManager(mLayoutManager);
        list.setAdapter(adapter);

//
        list.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();


                if (loading) {

                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = false;


                        if (COUNT > adapter.getItemCount())
                            getEvents(PAGE);

                    }
                }
            }
        });



        swipeRefreshLayout = (SwipeRefreshLayout)
                rootView.findViewById(R.id.refresh);

        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorPrimary,
                R.color.colorPrimary
        );



        getEvents(PAGE);


        return rootView;
    }



    public List<Event_c> getData(){

        List<Event_c> list = new ArrayList<Event_c>();
        Event_c st = new Event_c();
        list.add(st);
        return list;

    }


    @Override
    public void itemClicked(View view, int position) {



        if(ServiceHandler.isNetworkAvailable(getContext()))
        {

        Event_c event_c = adapter.getItem(position);



        if(event_c!=null) {
            Intent intent = new Intent(getActivity(), EventActivity.class);


            intent.putExtra(Event_c.Tags.ID, event_c.getId());
            intent.putExtra(Event_c.Tags.NAME, event_c.getName());
            intent.putExtra(Event_c.Tags.ADDRESS, event_c.getAddress());
            intent.putExtra(Event_c.Tags.DISTANCE, event_c.getDistance());
            intent.putExtra(Event_c.Tags.LAT, event_c.getLat());
            intent.putExtra(Event_c.Tags.LONG, event_c.getLng());
            intent.putExtra(Event_c.Tags.STATUS, event_c.getStatus());
            intent.putExtra(Event_c.Tags.TYPE, event_c.getType());
            intent.putExtra(Event_c.Tags.PHONE, event_c.getTel());
            intent.putExtra(Event_c.Tags.DESCRIPTION,event_c.getDescription());
            intent.putExtra(Event_c.Tags.WEBSITE,event_c.getWebSite());
            intent.putExtra(Event_c.Tags.TEL,event_c.getTel());
            intent.putExtra(Event_c.Tags.DATE_B,event_c.getDateB());
            intent.putExtra(Event_c.Tags.DATE_E,event_c.getDateE());



            try {



                intent.putExtra(Event_c.Tags.LISTIMAGES,event_c.getImageJson().toString());


            }catch (Exception e){e.printStackTrace();}





            startActivity(intent);



        }
        }else
        {
            Toast.makeText(getContext(),"Enable Network !!  ",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void iconImageViewOnClick(View v, int position) {


        Event_c event_c = adapter.getItem(position);

        if(event_c!=null) {
            int CurrentStore =  event_c.getId();
            final CustomBaseDialog dialog = new CustomBaseDialog(getContext(),CurrentStore);
            dialog.show();
            dialog.setCanceledOnTouchOutside(true);
        }else
        {
            Toast.makeText(getContext(),"Event not checked !!",Toast.LENGTH_LONG).show();

        }
    }


    private String search ="";
    public void getEvents(final int page){


        mGPS = new GPStracker(getActivity());

        swipeRefreshLayout.setRefreshing(true);

        SimpleRequest request = new SimpleRequest(Request.Method.POST,
                Constances.API.API_USER_GET_EVENTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{




                    JSONObject jsonObject = new JSONObject(response);
                   Log.e("response", jsonObject.toString());

                    //Log.e("response",response);

                    final EventParser mEventParser = new EventParser(jsonObject);

                    COUNT = mEventParser.getIntAttr(Tags.COUNT);




                    if(page==1)
                    {


                        Log.e("count",COUNT+"");

                        (new Handler()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                List<Event_c> list = mEventParser.getEvents();
                                    adapter.removeAll();
                                    for(int i=0;i<list.size();i++){
                                        //GET THE ID EVENT FROM EVENT LIKE IN DATABASE AND COMPARE IT WITH THIS ID

                                         if(isLiked == 1)
                                         {
                                           List<EventLiked> myEvent =  database.getLikedEvent();

                                             for(int j = 0 ;j<myEvent.size();j++)
                                             {
                                                 if(myEvent.get(j).getID_EVENT() == list.get(i).getId())
                                                 {
                                                     adapter.addItem(list.get(i));
                                                 }
                                             }
                                         }else
                                         {
                                             adapter.addItem(list.get(i));
                                         }


                                        Log.e("EventParser", "id event "+list.get(i).getId()+"   address   "+list.get(i).getAddress());
                                    }

                                swipeRefreshLayout.setRefreshing(false);
                                mViewManager.showResult();
                                loading = true;

                                if(COUNT>adapter.getItemCount())
                                    PAGE++;
                                if(COUNT==0){
                                    mViewManager.empty();
                                }

                            }
                        }, 800);
                    }
                    else
                    {
                        (new Handler()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                List<Event_c> list = mEventParser.getEvents();

                                for (int i = 0; i < list.size(); i++) {
                                    adapter.addItem(list.get(i));
                                    Log.e("EventParser", "id event "+list.get(i).getId()+"   address   "+list.get(i).getAddress());
                                }
                                swipeRefreshLayout.setRefreshing(false);
                                mViewManager.showResult();
                                loading = true;
                                if(COUNT>adapter.getItemCount())
                                    PAGE++;

                                if(COUNT==0){
                                    mViewManager.empty();
                                }
                            }
                        }, 800);

                    }

                }catch (JSONException e){
                    //send a rapport to support
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());

                mViewManager.error();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                if(mGPS.canGetLocation()){
                    params.put("lat",mGPS.getLatitude()+"");
                    params.put("lng",mGPS.getLongitude()+"");


                }



                params.put("token", Utils.getToken(getActivity()));
                params.put("mac_adr", ServiceHandler.getMacAddress(getActivity()));

                params.put("limit","12");


                params.put("page", page + "");
                params.put("search",search);

                Log.e("ListEventFragment","  params getEvent :"+params.toString());

                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(SimpleRequest.TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);



    }


    @Override
    public void onRefresh() {


        getEvents(1);
        PAGE = 1;



    }

    @Override
    public void customErrorView(View v) {

        Button retry = (Button) v.findViewById(R.id.trybtn);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mGPS = new GPStracker(getActivity());

                if(!mGPS.canGetLocation()&& listType==1)
                    mGPS.showSettingsAlert();

                getEvents(1);
                PAGE = 1;
                mViewManager.loading();
            }
        });

    }

    @Override
    public void customLoadingView(View v) {




    }

    @Override
    public void customEmptyView(View v) {


        Button btn= (Button) v.findViewById(R.id.trybtn);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mViewManager.loading();
                getEvents(1);
                PAGE = 1;
            }
        });



    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.action_map).setVisible(false);




    }
}
