package com.droideve.apps.smartgeostore.parser.api_parser;


import android.util.Log;

import com.droideve.apps.smartgeostore.classes.Event_c;
import com.droideve.apps.smartgeostore.classes.Images;
import com.droideve.apps.smartgeostore.parser.Parser;
import com.droideve.apps.smartgeostore.parser.tags.Tags;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by idriss on 1/12/2016.
 */
public class EventParser extends Parser {

    public EventParser(JSONObject json) {
        super(json);
    }

    public List<Event_c> getEvents(){

        List<Event_c> list = new ArrayList<Event_c>();

        try{

            JSONObject json_array = json.getJSONObject(Tags.RESULT);
            Log.e("JSONEventArray",json.toString());

            for (int i=0;i<json_array.length();i++){


                try {

                    JSONObject json_user = json_array.getJSONObject(i + "");

                    Log.e("EventUD",json_user.getInt("id_event")+"");
                    Event_c event  = new Event_c();
                    event.setId(json_user.getInt("id_event"));
                    event.setName(json_user.getString("name"));
                    event.setAddress(json_user.getString("address"));
                    event.setLat(json_user.getDouble("lat"));
                    event.setLng(json_user.getDouble("lng"));
                   // store.setType(json_user.getInt("type"));
                    event.setStatus(json_user.getInt("status"));
                    event.setDistance(json_user.getDouble("distance"));
                    event.setTel(json_user.getString("tel"));
                    event.setDateB(json_user.getString("date_b"));
                    event.setDateE(json_user.getString("date_e"));
                    event.setDescription(json_user.getString("description"));
                    event.setWebSite(json_user.getString("website"));




                /*if(!json_user.isNull("detail") && json_user.has("detail"))
                    store.setDetail(json_user.getJSONObject("detail"));
                else
                    store.setDetail(new JSONObject(""));
                    */


                    String jsonValues= "";
                    try {
                        if (!json_user.isNull("images")) {
                            jsonValues = json_user.getJSONObject("images").toString();
                            JSONObject jsonObject = new JSONObject(jsonValues);
                            ImagesParser imgp = new ImagesParser(jsonObject);
                            event.setListImages(imgp.getImagesList());
                            event.setImageJson(json_user.toString());
                        }

                    }catch (JSONException jex){
                        event.setListImages(new ArrayList<Images>());
                    }


                    Log.e("ParserEvent",event.getId()+"  "+event.getAddress()+"   "+event.getWebSite());
                    list.add(event);
                }catch (JSONException e){
                    e.printStackTrace();
                }

            }

        }catch (JSONException e){
            e.printStackTrace();
        }


        return list;
    }



}
