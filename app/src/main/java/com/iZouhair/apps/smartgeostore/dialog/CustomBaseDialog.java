package com.droideve.apps.smartgeostore.dialog;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.droideve.apps.smartgeostore.Constances;
import com.droideve.apps.smartgeostore.R;
import com.droideve.apps.smartgeostore.adapter.lists.CommentsListAdapter;
import com.droideve.apps.smartgeostore.classes.Comments;
import com.droideve.apps.smartgeostore.network.ServiceHandler;
import com.droideve.apps.smartgeostore.network.VolleySingleton;
import com.droideve.apps.smartgeostore.network.api_request.SimpleRequest;
import com.droideve.apps.smartgeostore.parser.api_parser.CommentParser;
import com.droideve.apps.smartgeostore.parser.tags.Tags;
import com.flyco.animation.BaseAnimatorSet;
import com.flyco.animation.BounceEnter.BounceTopEnter;
import com.flyco.animation.SlideExit.SlideBottomExit;
import com.flyco.dialog.utils.CornerUtils;
import com.flyco.dialog.widget.base.BaseDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by idriss on 18/07/2016.
 */


public class CustomBaseDialog extends BaseDialog<CustomBaseDialog> {
    private TextView tv_cancel;
    private BaseAnimatorSet mBasIn;
    private BaseAnimatorSet mBasOut;
    private CommentsListAdapter adapter;
    private int COUNT = 0,Store_id = 0;
    RequestQueue queue;
    LinearLayoutManager mLayoutManager;
    private RecyclerView list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    public CustomBaseDialog(Context context,int store) {
        super(context);
        //this.adapter = adapter;
        Store_id = store;
    }

    @Override
    public View onCreateView() {


        queue = VolleySingleton.getInstance(getContext()).getRequestQueue();
        mBasIn = new BounceTopEnter();
        mBasOut = new SlideBottomExit();


        widthScale(0.85f);
        showAnim(mBasIn);
        dismissAnim(mBasOut);

        // dismissAnim(this, new ZoomOutExit());
        View inflate = View.inflate(mContext, R.layout.dialog_comment, null);

        list = (RecyclerView) inflate.findViewById(R.id.list);
        mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        list.setLayoutManager(mLayoutManager);




        adapter = new CommentsListAdapter(getData(),getContext());


        tv_cancel = ViewFindUtils.find(inflate, R.id.tv_cancel);

        list.setItemAnimator(new DefaultItemAnimator());
        list.setAdapter(adapter);

        getComment();



        inflate.setBackgroundDrawable(
                CornerUtils.cornerDrawable(Color.parseColor("#ffffff"), dp2px(5)));


        return inflate;

    }

    @Override
    public void setUiBeforShow() {
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


    public List<Comments> getData(){

        List<Comments> list = new ArrayList<Comments>();
        Comments st = new Comments();

        Log.e("getDataComment",st.getReviewer()+" "+st.getComment());
        list.add(st);
        return list;

    }



    public void getComment()
    {

        SimpleRequest request = new SimpleRequest(Request.Method.POST,
                Constances.API.API_USER_GET_COMMENTS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try{

                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("_Comments", jsonObject.toString());



                    //Log.e("response",response);

                    final CommentParser commentParser = new CommentParser(jsonObject);

                    COUNT = commentParser.getIntAttr(Tags.COUNT);




                    Log.e("count",COUNT+"");

                    (new Handler()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            List<Comments> list = commentParser.getComments();
                            adapter.removeAll();


                            for (int i = 0; i < list.size(); i++) {
                                adapter.addItem(list.get(i));

                                Log.e("commentParser",adapter.getItem(i).getComment()+"  "+adapter.getItem(i).getReviewer());
                            }


                        }

                    },800);

                }catch (JSONException e){
                    //send a rapport to support
                    e.printStackTrace();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());


            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                params.put("store_id",Store_id+"");
                params.put("mac_adr", ServiceHandler.getMacAddress(getContext()));



                Log.e("ListStoreFragment","  params getStores :"+params.toString());

                return params;
            }

        };


        request.setRetryPolicy(new DefaultRetryPolicy(SimpleRequest.TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);



    }

    }


