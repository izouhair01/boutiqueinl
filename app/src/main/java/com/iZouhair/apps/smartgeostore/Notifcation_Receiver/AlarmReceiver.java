package com.droideve.apps.smartgeostore.Notifcation_Receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.droideve.apps.smartgeostore.EventLikedActivity;
import com.droideve.apps.smartgeostore.R;

import java.util.Random;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {


        Bundle bundle = intent.getExtras();


        if(bundle != null)
        {
           String  Message = bundle.getString("message");
           String  Date = bundle.getString("dateEvent");

            Notification(context,Message,Date);
            Log.e("AlarmReviever","Alarm Enabled");


        }else
        {
            Log.e("AlarmReviever","Alarm not Set");
        }



        }


    public void Notification(Context context, String message,String date ) {




        // Set Notification Title
        String strtitle = context.getString(context.getApplicationInfo().labelRes);
        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, EventLikedActivity.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context)
                // Set Icon
                .setSmallIcon(R.mipmap.ic_launcher)
                // Set Ticker Message
                //.setTicker(message)
                // Set Title
                .setContentTitle(strtitle)
                .setSubText(message)
                // Set Text
                .setContentText("UpComming event :"+date)
                // Add an Action Button below Notification
               // .addAction(R.drawable.ic_launcher, "Action Button", pIntent)
                // Set PendingIntent into Notification
                .setDefaults(Notification.DEFAULT_SOUND)

                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        notificationmanager.notify(m, builder.build());

    }



}

